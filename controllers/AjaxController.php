<?php

class AjaxController extends McecomController {
	
	public function actionGetProductPrice() {
		if ($this->isRequestDataSet('Order', 'product_id') ) {
			$model = Product::model()->findByPk($_POST['Order']['product_id']);
			$price = ($model) ? number_format($model->price, 2) : '0.00';	
		} else {
			$price = '0.00';
		} // if
		
		$this->renderPartial('update-value', array(
			'id' => 'Order_product_price',
			'value' => $price,
		));
	} // function
	
	public function actionGetDeliveryPrice() {
		if ($this->isRequestDataSet('Order', 'delivery_id') ) {
			$model = Delivery::model()->findByPk($_POST['Order']['delivery_id']);
			$price = ($model) ? number_format($model->price, 2) : '0.00';	
		} else {
			$price = '0.00';
		} // if
		
		$this->renderPartial('update-value', array(
			'id' => 'Order_delivery_price',
			'value' => $price,
		));
	} // function
	
	protected function isRequestDataSet($form = 'undefined', $field = 'undefined') {
		if (!isset($_POST) ) {
			return false;
		} // if
		
		if (!isset($_POST[$form]) ) {
			return false;
		} // if
		
		if (!isset($_POST[$form][$field]) ) {
			return false;
		} // if
		
		return true;
	} // function
	
} // class
