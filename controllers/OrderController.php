<?php

class OrderController extends McecomController {
	
	public function actionCreate() {
		$order = new Order;
		$customers = Customer::model()->findAll();
		$deliveries = Delivery::model()->findAll();
		$products = Product::model()->findAll();
		
		if ($this->isFormSubmitted() ) {
			$order->attributes = $this->request['Order'];
			$order->date_placed = date('Y-m-d H:i:s');
			
			if ($order->save() ) {
				Yii::app()->user->setFlash('success', 'Order Placed');
			
				$this->redirect($this->createUrl('home/index'));
			} else {
				Yii::app()->user->setFlash('error', 'There were errors with your form');
			} // if
		} // if
		
		$this->render('order-form', array(
			'model' => $order,
			'customers' => $customers,
			'deliveries' => $deliveries,
			'products' => $products,
		));
	} // function
		
	public function actionList() {
		$orders = Order::model()->with('customer')->findAll();
		
		$this->render('order-listing', array(
			'orders' => $orders,
		));
	} // function
	
	public function actionDetail($id) {
		$order = Order::model()->with(array(
			'customer',
			'product',
			'delivery',
		))->findByPk($id);
		
		if (!$order) {
			Yii::app()->user->setFlash('error', 'Could not find selected order information');
			
			$this->redirect($this->createUrl('order/list'));
		} // if
		
		$this->render('order-detail', array(
			'order' => $order,
		));
	} // function
	
} // class
