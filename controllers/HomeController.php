<?php

class HomeController extends McecomController {
	
	public function actionIndex() {
		$this->pageTitle = Yii::app()->name.' - Menu';
		$this->render('index');
	} // function
	
} // class
