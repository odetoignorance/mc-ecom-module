# README #

1. To be imported in to a Yii project as a module
2. To access this module, add it to the modules list in the main config file (same name as the module folder name). EG:
	'modules' => array(
		'mcecom'
	)
3. Import the database schema file to your database and set up a database connection in the config file

4. Optional: If you are using tidy URLs, then you can add the following code to your UrlManager rules:
	'mcecom/' => 'mcecom/home/index',
	'mcecom/<action>' => 'mcecom/home/<action>',
	'mcecom/<controller>/<action>' => 'mcecom/<controller>/<action>',
	
	Then you can access the module by going to yoursitename/mcecom
	Otherwise you can access it by going to yoursitename/index.php?r=mcecom/home

### What is this repository for? ###

+ Version 1.01.20140718.1546
    * Added some styling to the front page navigation
    * Added styling and error reporting to the order creation form, as it was a bit too basic in style to use as another code test example
+ Version 1.00.20140618.1335
    * A *very* quick module created for Midland Computers code test. Does not have any styling added, and is pure functionality at this stage. Uploaded and made public in order to let MC see the code created for the completion of their task