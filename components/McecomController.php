<?php

/**
 * Extends the base project controller, allowing side-wide functions to be used
 * 
 * All controllers in this module should extends this controller
 */
class McecomController extends Controller {
	protected $request;
	
	protected function isFormSubmitted() {
		if (isset($_POST) && count($_POST) > 0) {
			$this->request = $_POST;
			
			return true;
		} // if
		
		return false;
	} // function
} // class
