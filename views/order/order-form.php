<? echo Yii::app()->controller->module->registerCss('main.css'); ?>
<?php
	foreach(Yii::app()->user->getFlashes() as $key => $message) {
		echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	} // foreach
?>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'order-form',
	'focus' => array($model, 'customer_id'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class='row'>
	<div class='input-wrap'>
		<?php echo $form->labelEx($model, 'customer_id'); ?>
		<?php echo $form->dropDownList($model, 'customer_id', CHtml::listData($customers, 'id', 'fullName'), array('empty' => 'Select Customer')); ?>
		<?php echo $form->error($model, 'customer_id'); ?>
	</div>
</div>

<div class='row'>
	<div class='input-wrap'>
		<?php echo $form->labelEx($model, 'product_id'); ?>
		<?php echo $form->dropDownList(
			$model, 
			'product_id', 
			CHtml::listData($products, 'id', 'name'), 
			array(
				'empty' => 'Select Product',
				'ajax' => array(
					'type' => 'POST',
					'url' => $this->createUrl('ajax/getProductPrice'),
					'update' => '#Order_product_price',
				),
			)
		); ?>
		<?php echo $form->error($model, 'product_id'); ?>
	</div>
	<div class='input-wrap'>
		<?php echo $form->labelEx($model, 'product_price'); ?>
		<span class='prefix'>&pound;</span><?php echo $form->textField($model, 'product_price'); ?>
		<?php echo $form->error($model, 'product_price'); ?>
	</div>
</div>

<div class='row'>
	<div class='input-wrap'>
		<?php echo $form->labelEx($model, 'delivery_id'); ?>
		<?php echo $form->dropDownList(
			$model, 
			'delivery_id', 
			CHtml::listData($deliveries, 'id', 'name'), 
			array(
				'empty' => 'Select Delivery',
				'ajax' => array(
					'type' => 'POST',
					'url' => $this->createUrl('ajax/getDeliveryPrice'),
					'update' => '#Order_delivery_price',
				),
			)
		); ?>
		<?php echo $form->error($model, 'delivery_id'); ?>
	</div>
	<div class='input-wrap'>
		<?php echo $form->labelEx($model, 'delivery_price'); ?>
		<span class='prefix'>&pound;</span><?php echo $form->textField($model, 'delivery_price'); ?>
		<?php echo $form->error($model, 'delivery_price'); ?>
	</div>
</div>

<div class='row'>
	<?php echo CHtml::submitButton('Place Order', array('class' => 'button') ); ?>
	<a class='button' href='<?php echo $this->createUrl('home/index'); ?>'>Back</a>
</div>

<?php $this->endWidget(); ?>


