<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Date</th>
			<th>Name</th>
			<th>Address</th>
			<th>Total</th>
			<th>&nbsp</th>
		</tr>
	</thead>
	<tbody>
		<?php if (count($orders) <= 0) { ?>
			<tr>
				<td colspan='6'>No orders placed yet</td>
			</tr>
		<?php } else { ?>
			<?php foreach ($orders as $order) { ?>
				<tr>
					<td><?php echo $order->id; ?></td>
					<td><?php echo date('d/m/y - H:i', strtotime($order->date_placed) ); ?></td>
					<td><?php echo $order->customer->fullName; ?></td>
					<td><?php echo $order->customer->fullAddress; ?></td>
					<td>&pound;<?php echo $order->total; ?></td>
					<td><a href='<?php echo $this->createUrl('order/detail', array('id'=>$order->id) );?>'>View Details</a></td>
				</tr>
			<?php } // foreach ?>
		<?php } // if ?>
	</tbody>
</table>

<a href='<?php echo $this->createUrl('home/index'); ?>'>Back</a>
