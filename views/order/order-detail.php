<table>
	<tr>
		<th>ID</th>
		<td><?php echo($order->id); ?></td>
	</tr>
	<tr>
		<th>Date</th>
		<td><?php echo date('d/m/y - H:i', strtotime($order->date_placed) ); ?></td>
	</tr>
	<tr>
		<th>Total</th>
		<td>&pound;<?php echo number_format($order->total, 2); ?></td>
	</tr>
	<tr>
		<th>Name</th>
		<td><?php echo($order->customer->fullName); ?></td>
	</tr>
	<tr>
		<th>Address</th>
		<td><?php echo($order->customer->fullAddress); ?></td>
	</tr>
	<tr>
		<th>Product ID</th>
		<td><?php echo($order->product_id); ?></td>
	</tr>
	<tr>
		<th>Product Name</th>
		<td><?php echo($order->product->name); ?></td>
	</tr>
	<tr>
		<th>Product Price</th>
		<td>&pound;<?php echo number_format($order->product_price, 2); ?></td>
	</tr>
	<tr>
		<th>Delivery ID</th>
		<td><?php echo($order->delivery_id); ?></td>
	</tr>
	<tr>
		<th>Delivery Name</th>
		<td><?php echo($order->delivery->name); ?></td>
	</tr>
	<tr>
		<th>Delivery Price</th>
		<td>&pound;<?php echo number_format($order->delivery_price, 2); ?></td>
	</tr>
</table>

<a href='<?php echo $this->createUrl('order/list'); ?>'>Back</a>