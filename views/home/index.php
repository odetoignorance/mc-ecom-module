<?php echo Yii::app()->controller->module->registerCss('main.css'); ?>
<?php
	foreach(Yii::app()->user->getFlashes() as $key => $message) {
		echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	} // foreach
?>
<p>Choose an action below</p>
<div class='menu-tiles'>
	<div class='menu-tile'>
		<a href='<?php echo $this->createUrl('order/create');?>'>New Order</a>
	</div>
	<div class='menu-tile'>
		<a href='<?php echo $this->createUrl('order/list');?>'>View All Orders</a>
	</div>
</div>
