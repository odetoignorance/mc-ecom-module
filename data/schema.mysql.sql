-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 18, 2014 at 01:32 PM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `mc_ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `building` varchar(64) NOT NULL,
  `street` varchar(64) NOT NULL,
  `area` varchar(64) NOT NULL,
  `city` varchar(64) NOT NULL,
  `country` varchar(64) NOT NULL,
  `postcode` varchar(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `first_name`, `last_name`, `building`, `street`, `area`, `city`, `country`, `postcode`) VALUES
(1, 'Ian', 'Naish', '6', 'Cooke Drive', 'Stirchley', 'Telford', 'UK', 'TF43SU'),
(2, 'John', 'Smith', '221B', 'Baker Street', '', 'London', 'UK', 'NW16XE');

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE IF NOT EXISTS `delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `delivery`
--

INSERT INTO `delivery` (`id`, `name`, `description`, `price`) VALUES
(1, 'Collection', 'Collection', 0),
(2, 'Standard', 'Standard 3-5 day delivery', 2.99),
(3, 'Next Day', 'Next day delivery (orders placed after 4pm will ship the following day)', 5.99);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_price` float NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `delivery_price` float NOT NULL,
  `date_placed` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`,`delivery_price`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `customer_id`, `product_id`, `product_price`, `delivery_id`, `delivery_price`, `date_placed`) VALUES
(1, 1, 1, 8, 3, 5.99, '2014-06-18 12:51:44'),
(2, 2, 2, 32.61, 1, 0, '2014-06-18 12:54:15');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `stock` int(5) NOT NULL DEFAULT '0',
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `stock`, `price`) VALUES
(1, 'Example Product 1', 'This is an example product. Isn''t it nice!', 99, 8),
(2, 'Example Product 2', 'Another example product. This one costs more though!', 99, 19.95);
